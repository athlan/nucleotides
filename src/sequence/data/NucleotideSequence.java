package sequence.data;

/**
 * Klasa representuje sekwencje nukleotydow.
 *
 */
public class NucleotideSequence implements SequenceInterface {
	
	private String sequenceName;
	
	private char[] sequencedNucleotides;

	public NucleotideSequence(String sequenceName, char[] sequencedNucleotides) {
		super();
		this.sequenceName = sequenceName;
		this.sequencedNucleotides = sequencedNucleotides;
	}

	public String getSequenceName() {
		return sequenceName;
	}

	public char[] getSequencedNucleotides() {
		return sequencedNucleotides;
	}

	@Override
	public char[] getSequencedChain() {
		return getSequencedNucleotides();
	}
}
