package sequence.data.statistics;

import java.util.HashMap;
import java.util.Map;

import sequence.data.NucleotideSequence;

/**
 * Klasa oblicza staytystyke nukleotydow w sekwencji.
 *
 */
public class NucleotideSequenceNucleotideHistogramStatistic {
	
	public Map<Character, Integer> getNucleotideHistogramStatistic(NucleotideSequence input) {
		Map<Character, Integer> histogram = new HashMap<>();
		
		for(char nucleotide : input.getSequencedNucleotides()) {
			Character nucleotideCode = Character.valueOf(nucleotide);
			Integer stat = 1;
			
			if(histogram.containsKey(nucleotideCode)) {
				stat = new Integer(histogram.get(nucleotideCode).intValue() + 1);
			}
			else {
				stat = 1;
			}
			
			histogram.put(nucleotideCode, stat);
		}
		
		return histogram;
	}
}
