package sequence.data.statistics;

import java.util.HashMap;
import java.util.Map;

import sequence.data.AminoacidSequence;

/**
 * Klasa oblicza staytystyke par aminokwasow w sekwencji.
 *
 */
public class AminoacidSequenceNucleotidePairsStatistic {
	
	public Map<String, Integer> getNucleotidePairsStatistic(AminoacidSequence input) {
		Map<String, Integer> histogram = new HashMap<>();
		
		String nucleotideChain = new String(input.getSequencedAminoacids());
		nucleotideChain = nucleotideChain.replace("N", "");
		
		for(int i = 1, j = nucleotideChain.length(); i < j; i++) {
			String pair = nucleotideChain.substring(i-1, i+1);
			Integer stat = 1;
			
			if(histogram.containsKey(pair)) {
				stat = new Integer(histogram.get(pair).intValue() + 1);
			}
			else {
				stat = 1;
			}
			
			histogram.put(pair, stat);
		}
		
		return histogram;
	}
}
