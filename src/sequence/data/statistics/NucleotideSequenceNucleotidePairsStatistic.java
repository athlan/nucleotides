package sequence.data.statistics;

import java.util.HashMap;
import java.util.Map;

import sequence.data.NucleotideSequence;

/**
 * Klasa oblicza staytystyke par nukleotydow w sekwencji.
 *
 */
public class NucleotideSequenceNucleotidePairsStatistic {
	
	public Map<String, Integer> getNucleotidePairsStatistic(NucleotideSequence input) {
		Map<String, Integer> histogram = new HashMap<>();
		
		String nucleotideChain = new String(input.getSequencedNucleotides());
		nucleotideChain = nucleotideChain.replace("N", "");
		
		for(int i = 1, j = nucleotideChain.length(); i < j; i++) {
			String pair = nucleotideChain.substring(i-1, i+1);
			Integer stat = 1;
			
			if(histogram.containsKey(pair)) {
				stat = new Integer(histogram.get(pair).intValue() + 1);
			}
			else {
				stat = 1;
			}
			
			histogram.put(pair, stat);
		}
		
		return histogram;
	}
}
