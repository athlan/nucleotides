package sequence.data;

/**
 * Klasa representuje sekwencje aminokwasow.
 * 
 */
public class AminoacidSequence implements SequenceInterface {
	
	private String sequenceName;
	
	private char[] sequencedAminoacids;

	public AminoacidSequence(String sequenceName, char[] sequencedAminoacids) {
		super();
		this.sequenceName = sequenceName;
		this.sequencedAminoacids = sequencedAminoacids;
	}

	public String getSequenceName() {
		return sequenceName;
	}

	public char[] getSequencedAminoacids() {
		return sequencedAminoacids;
	}

	@Override
	public char[] getSequencedChain() {
		return getSequencedAminoacids();
	}
}
