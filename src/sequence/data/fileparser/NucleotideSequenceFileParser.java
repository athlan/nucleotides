package sequence.data.fileparser;

import java.io.File;
import java.util.List;

import sequence.data.SequenceInterface;

/**
 * Interfejs definiujacy jednolity dostep do plikow z sekwencjami nukleotydow
 * i aminokwasow, ktore roznia sie sposobem reprezentacji.
 *
 */
public interface NucleotideSequenceFileParser {

	public List<SequenceInterface> readNucleotideSequenceFromFile(File sourceFile) throws Exception;
}
