package sequence.data.fileparser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import sequence.data.AminoacidSequence;
import sequence.data.NucleotideSequence;
import sequence.data.NucleotideValidator;
import sequence.data.SequenceInterface;
import sequence.data.SequenceInterface;

/**
 * Klasa odczytuje sekwencje nukleotydow i aminokwasow z plikow FASTA
 *
 */
public class FastaNucleotideSequenceFileParser implements NucleotideSequenceFileParser {

	@Override
	public List<SequenceInterface> readNucleotideSequenceFromFile(File sourceFile) throws Exception {
		List<SequenceInterface> list = new ArrayList<>();
		
		try (BufferedReader br = new BufferedReader(new FileReader(sourceFile))) {
		    String line;
		    
		    String sequenceName = null;
		    List<Character> sequencedNucleotides = null;
		    
		    while ((line = br.readLine()) != null) {
		    	// process the line.
		    	
		    	// new item
		    	if(line.startsWith(">")) {
		    		if(sequenceName != null && sequencedNucleotides != null) {
				    	list.add(createItem(sequenceName, sequencedNucleotides));
				    }
		    		
		    		String[] line_split = line.split("\\|");
		    		
		    		if(line_split.length >= 5) {
		    			sequenceName = line_split[4];
		    		}
		    		else {
		    			sequenceName = "Undefined";
		    		}
		    		
		    		sequencedNucleotides = new ArrayList<>();
		    	}
		    	else {
		    		if(sequencedNucleotides != null) {
		    			for(char nucleotideChar : line.toCharArray()) {
//		    				if(!NucleotideValidator.validateNucleotideCharacter(nucleotideChar)) {
//		    					throw new Exception("Invalid nucleotide character: " + nucleotideChar);
//		    				}
		    				
		    				sequencedNucleotides.add(Character.valueOf(nucleotideChar));
		    			}
		    		}
		    	}
		    }
		    
		    if(sequenceName != null && sequencedNucleotides != null) {
		    	list.add(createItem(sequenceName, sequencedNucleotides));
		    }
		} catch (FileNotFoundException e) {
			throw new Exception(e);
		} catch (IOException e) {
			throw new Exception(e);
		}
		
		return list;
	}
	
	private SequenceInterface createItem(String sequenceName, List<Character> sequencedNucleotides) {
		char[] sequencedNucleotidesBuffer = new char[sequencedNucleotides.size()];
		
		int i = 0;
		for(Character sequencedNucleotide : sequencedNucleotides) {
			sequencedNucleotidesBuffer[i] = sequencedNucleotide.charValue();
			++i;
		}

		if(NucleotideValidator.validateNucleotideCharacters(sequencedNucleotidesBuffer)) {
			return new NucleotideSequence(sequenceName, sequencedNucleotidesBuffer);
		}
		else {
			return new AminoacidSequence(sequenceName, sequencedNucleotidesBuffer);
		}
	}

}
