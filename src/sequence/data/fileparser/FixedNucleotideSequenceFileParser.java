package sequence.data.fileparser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import sequence.data.NucleotideSequence;
import sequence.data.SequenceInterface;

/**
 * Klasa podaje stale dane sekwencji nukleotydow i aminokwasow.
 * Wylacznie do testow interfeju uzytkownika.
 *
 */
public class FixedNucleotideSequenceFileParser implements NucleotideSequenceFileParser {

	@Override
	public List<SequenceInterface> readNucleotideSequenceFromFile(File sourceFile) throws Exception {
		List<SequenceInterface> list = new ArrayList<>();

		list.add(new NucleotideSequence("Sekwencja 1", "ATGACTTGTTACGCGCTTACTTTGGCTCTACTGGCGCTGGCCGGCTGCATTTGCTGTACGTTCGCACGGGCCACGCCCGACGAGGAGCGCTACGTGGAAA".toCharArray()));
		list.add(new NucleotideSequence("Sekwencja 2", "GCGCCCGTCCAACCACCGGGTCCGCCCTGCAAGTATCCATACTTTCTGGATAACTCACTTAACCCAAATATGCGGATGCCAAAGCGAAACTCGGAACTGA".toCharArray()));
		
		return list;
	}

}
