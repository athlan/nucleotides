package sequence.data.fileparser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import sequence.data.AminoacidSequence;
import sequence.data.NucleotideSequence;
import sequence.data.NucleotideValidator;
import sequence.data.SequenceInterface;

/**
 * Klasa odczytuje sekwencje nukleotydow i aminokwasow z plikow SEQ GenBank
 *
 */
public class GenbankNucleotideSequenceFileParser implements NucleotideSequenceFileParser {
	
	static enum ParsingState {
		UNDEFINED,
		LOCUS,
		DEFINITION,
		ORIGIN,
	}

	@Override
	public List<SequenceInterface> readNucleotideSequenceFromFile(File sourceFile) throws Exception {
		List<SequenceInterface> list = new ArrayList<>();
		
		try (BufferedReader br = new BufferedReader(new FileReader(sourceFile))) {
		    String line;
		    
		    String sequenceName = null;
		    List<Character> sequencedNucleotides = null;
		    
		    ParsingState parsingState = ParsingState.UNDEFINED;
		    
		    while ((line = br.readLine()) != null) {
		    	// process the line.
		    	
		    	if(line.startsWith("LOCUS")) {
		    		if(sequenceName != null && sequencedNucleotides != null) {
				    	list.add(createItem(sequenceName, sequencedNucleotides));
				    }
		    		
		    		parsingState = ParsingState.LOCUS;
		    		sequenceName = null;
		    		sequencedNucleotides = null;
		    	}
		    	else if(line.startsWith("DEFINITION")) {
		    		parsingState = ParsingState.DEFINITION;
		    		line = line.substring("DEFINITION".length());
		    		
		    		sequenceName = "";
		    	}
		    	else if(line.startsWith("ORIGIN")) {
		    		parsingState = ParsingState.ORIGIN;
		    		line = "";
		    		sequencedNucleotides = new ArrayList<>();
		    	}

	    		if(parsingState == ParsingState.DEFINITION) {
	    			if(!line.startsWith(" ")) {
	    				parsingState = ParsingState.LOCUS;
	    			}
	    			else {
	    				line = line.trim();
		    			sequenceName += line;
	    			}
	    		}
	    		if(parsingState == ParsingState.ORIGIN) {
	    			if(!line.startsWith(" ") && !line.equals("")) {
	    				parsingState = ParsingState.LOCUS;
	    			}
	    			else {
		    			if(null != sequencedNucleotides && line.length() > 0) {
		    				line = line.substring(10);
		    				line = line.replace(" ", "");
		    				line = line.toUpperCase();
		    				
			    			for(char nucleotideChar : line.toCharArray()) {
//			    				if(!NucleotideValidator.validateNucleotideCharacter(nucleotideChar)) {
//			    					throw new Exception("Invalid nucleotide character: " + nucleotideChar);
//			    				}
			    				
			    				sequencedNucleotides.add(Character.valueOf(nucleotideChar));
			    			}
		    			}
	    			}
	    		}
		    }
		    
		    if(sequenceName != null && sequencedNucleotides != null) {
		    	list.add(createItem(sequenceName, sequencedNucleotides));
		    }
		} catch (FileNotFoundException e) {
			throw new Exception(e);
		} catch (IOException e) {
			throw new Exception(e);
		}
		
		return list;
	}
	
	private SequenceInterface createItem(String sequenceName, List<Character> sequencedNucleotides) {
		char[] sequencedNucleotidesBuffer = new char[sequencedNucleotides.size()];
		
		int i = 0;
		for(Character sequencedNucleotide : sequencedNucleotides) {
			sequencedNucleotidesBuffer[i] = sequencedNucleotide.charValue();
			++i;
		}
		
		if(NucleotideValidator.validateNucleotideCharacters(sequencedNucleotidesBuffer)) {
			return new NucleotideSequence(sequenceName, sequencedNucleotidesBuffer);
		}
		else {
			return new AminoacidSequence(sequenceName, sequencedNucleotidesBuffer);
		}
	}

}
