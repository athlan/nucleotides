package sequence.data;

/**
 * Klasa sluzy do weryfikacji, czy skladowe z sekwencji nukleotydow
 * sa prawidlowe.
 *
 */
public class NucleotideValidator {
	
	/**
	 * Sprawdza, czy dany nukleotyd jest poprawnie oznaczony.
	 * 
	 * @param nucleotideCharacter
	 * @return true, jezeli dany nukleotyd jest poprawnie oznaczony
	 */
	public static boolean validateNucleotideCharacter(char nucleotideCharacter) {
		char c = nucleotideCharacter;
		
		if(c == 'A' || c == 'G' || c == 'T' || c == 'C' || c == 'N') {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Sprawdza, czy dana sekwencja nukleotydow jest poprawnie oznaczona.
	 * 
	 * @param nucleotideCharacters
	 * @return true, jezeli dana sekwencja nukleotydow jest poprawnie oznaczona
	 */
	public static boolean validateNucleotideCharacters(char[] nucleotideCharacters) {
		for(char nucleotideCharacter : nucleotideCharacters) {
			if(!validateNucleotideCharacter(nucleotideCharacter)) {
				return false;
			}
		}
		
		return true;
	}
}
