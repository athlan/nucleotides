package sequence.data;

/**
 * Interfejs definiuje sekwencje nukleotydow lub aminokwaow.
 *
 */
public interface SequenceInterface {
	
	public String getSequenceName();

	public char[] getSequencedChain();
}
