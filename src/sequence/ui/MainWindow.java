package sequence.ui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import sequence.data.AminoacidSequence;
import sequence.data.NucleotideSequence;
import sequence.data.SequenceInterface;
import sequence.data.fileparser.FastaNucleotideSequenceFileParser;
import sequence.data.fileparser.FixedNucleotideSequenceFileParser;
import sequence.data.fileparser.GenbankNucleotideSequenceFileParser;
import sequence.data.fileparser.NucleotideSequenceFileParser;
import sequence.data.statistics.AminoacidSequenceNucleotideHistogramStatistic;
import sequence.data.statistics.AminoacidSequenceNucleotidePairsStatistic;
import sequence.data.statistics.NucleotideSequenceNucleotideHistogramStatistic;
import sequence.data.statistics.NucleotideSequenceNucleotidePairsStatistic;

public class MainWindow {

	private Logger log = Logger.getLogger(MainWindow.class.getName());
	
	private JFrame frame;

	private JFileChooser fileChooserFasta;
	private JFileChooser fileChooserGenbank;
	private JFileChooser fileChooserSaveResult;

	private NucleotideSequenceFileParser NucleotideSequenceFileParserFasta;
	private NucleotideSequenceFileParser NucleotideSequenceFileParserGenbank;
	
	private List<SequenceInterface> itemsListNucleotides;
	private NucleotidesListModel itemsListModel;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setExtendedState(Frame.MAXIMIZED_BOTH);
		
		itemsListNucleotides = Collections.emptyList();
//		try {
//			itemsListNucleotides = new FixedNucleotideSequenceFileParser().readNucleotideSequenceFromFile(new File(""));
//		} catch (Exception e1) {
//		}

		NucleotideSequenceFileParserFasta = new FastaNucleotideSequenceFileParser();
		NucleotideSequenceFileParserGenbank = new GenbankNucleotideSequenceFileParser();

		fileChooserFasta = new JFileChooser();
		fileChooserFasta.setFileFilter(new FileNameExtensionFilter("FASTA files", "fasta"));

		fileChooserGenbank = new JFileChooser();
		fileChooserGenbank.setFileFilter(new FileNameExtensionFilter("Genbank seq files", "seq"));

		fileChooserSaveResult = new JFileChooser();
		fileChooserSaveResult.setFileFilter(new FileNameExtensionFilter("TXT files", "txt"));
		
		JSplitPane splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.5);
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		frame.getContentPane().add(splitPane, BorderLayout.CENTER);
		
		JScrollPane scrollPane = new JScrollPane();
		splitPane.setRightComponent(scrollPane);
		
		JTextArea resultText = new JTextArea();
		resultText.setEditable(false);
		scrollPane.setViewportView(resultText);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		splitPane.setLeftComponent(scrollPane_1);
		
		JList itemsList = new JList();
		scrollPane_1.setViewportView(itemsList);

		itemsListModel = new NucleotidesListModel(itemsList);
//		itemsListModel = new NucleotidesListModel();
		itemsListModel.setList(itemsListNucleotides);
		
		itemsList.setModel(itemsListModel);
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnDane = new JMenu("Dane");
		menuBar.add(mnDane);
		
		JMenuItem mntmOtworzFasta = new JMenuItem("Otworz Fasta");
		mnDane.add(mntmOtworzFasta);
		mntmOtworzFasta.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent event) {
				int returnVal = fileChooserFasta.showOpenDialog(frame);
				
				if (returnVal == JFileChooser.APPROVE_OPTION) {
		            File file = fileChooserFasta.getSelectedFile();
            		
		            log.info("Opening: " + file.getName() + ".");
		            
		            try {
		            	itemsListNucleotides = NucleotideSequenceFileParserFasta.readNucleotideSequenceFromFile(file);
		            	itemsListModel.setList(itemsListNucleotides);
		            	JOptionPane.showMessageDialog(null, "Dane zostaly pobrane z pliku FASTA");
		            }
		            catch(Exception e) {
		            	JOptionPane.showMessageDialog(null, "Problem z otwarciem pliku FASTA: " + e.getMessage());
		            }
		        } else {
		            log.info("Open command cancelled by user.");
		        }
			}
		});
		
		JMenuItem mntmOtworzGenbank = new JMenuItem("Otworz GenBank");
		mnDane.add(mntmOtworzGenbank);
		
		mntmOtworzGenbank.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent event) {
				int returnVal = fileChooserGenbank.showOpenDialog(frame);
				
				if (returnVal == JFileChooser.APPROVE_OPTION) {
		            File file = fileChooserGenbank.getSelectedFile();
            		
		            log.info("Opening: " + file.getName() + ".");
		            
		            try {
		            	itemsListNucleotides = NucleotideSequenceFileParserGenbank.readNucleotideSequenceFromFile(file);
		            	itemsListModel.setList(itemsListNucleotides);
		            	JOptionPane.showMessageDialog(null, "Dane zostaly pobrane z pliku GenBank");
		            }
		            catch(Exception e) {
		            	JOptionPane.showMessageDialog(null, "Problem z otwarciem pliku GenBank: " + e.getMessage());
		            }
		        } else {
		            log.info("Open command cancelled by user.");
		        }
			}
		});

		JMenuItem mntmZapiszWynik = new JMenuItem("Zapisz wynik");
		mnDane.add(mntmZapiszWynik);
		
		mntmZapiszWynik.setEnabled(false);
		mntmZapiszWynik.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int returnVal = fileChooserSaveResult.showOpenDialog(frame);
				
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					String str = resultText.getText();
		            File file = fileChooserSaveResult.getSelectedFile();
		            
		            if (fileChooserSaveResult.getFileFilter() instanceof FileNameExtensionFilter) {
		                String[] exts = ((FileNameExtensionFilter)fileChooserSaveResult.getFileFilter()).getExtensions();
		                String nameLower = file.getName().toLowerCase();
		                
		                boolean isExtension = false;
		                for (String ext : exts) { // check if it already has a valid extension
		                    if (nameLower.endsWith('.' + ext.toLowerCase())) {
		                    	isExtension = true; // if yes, return as-is
		                    }
		                }
		                
		                // if not, append the first extension from the selected filter
		                if(!isExtension) {
		                	file = new File(file.toString() + '.' + exts[0]);
		                }
		            }
		            
		            FileWriter fw;
					try {
						fw = new FileWriter(file);
			            fw.write(str);
			            fw.close();
			            
			            JOptionPane.showMessageDialog(null, "Zapisano do pliku: " + file.getAbsolutePath());
					} catch (IOException e1) {
						e1.printStackTrace();
						
						JOptionPane.showMessageDialog(null, "Nie udalo sie zapisac do pliku: " + e1.getMessage());
					}
				}
			}
		});
		
		resultText.getDocument().addDocumentListener(new DocumentListener() {
			private void enableSave() {
				boolean hasText = resultText.getText().equals("");
				mntmZapiszWynik.setEnabled(!hasText);
			}
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				enableSave();
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				enableSave();
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				enableSave();
			}
		});
		
		JMenu mnObliczenia = new JMenu("Obliczenia");
		menuBar.add(mnObliczenia);
		
		JMenuItem mntmObliczNukleotyd = new JMenuItem("Oblicz nukleotyd");
		mnObliczenia.add(mntmObliczNukleotyd);
		mntmObliczNukleotyd.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int i = itemsList.getSelectedIndex();
				
				if(i == -1) {
					JOptionPane.showMessageDialog(null, "nie wybrano sekwencji na liscie");
				}
				else {
					NucleotideSequenceNucleotideHistogramStatistic stat = new NucleotideSequenceNucleotideHistogramStatistic();
					SequenceInterface input = itemsListNucleotides.get(i);
					
					if(input instanceof NucleotideSequence) {
						NucleotideSequence input2 = (NucleotideSequence)input;
						Map<Character, Integer> statData = stat.getNucleotideHistogramStatistic(input2);
						
						String statDataText = "Statystyka nukleotydow dla sekwencji " + input.getSequenceName() + " \n\n";
						for(Entry<Character, Integer> statDataEntry : statData.entrySet()) {
							statDataText += statDataEntry.getKey() + ": " + statDataEntry.getValue() + "\n";
						}
						
						resultText.setText(statDataText);
					}
				}
			}
		});
		
		JMenuItem mntmObliczAminokwas = new JMenuItem("Oblicz aminokwas");
		mnObliczenia.add(mntmObliczAminokwas);
		mntmObliczAminokwas.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int i = itemsList.getSelectedIndex();
				
				if(i == -1) {
					JOptionPane.showMessageDialog(null, "nie wybrano sekwencji na liscie");
				}
				else {
					AminoacidSequenceNucleotideHistogramStatistic stat = new AminoacidSequenceNucleotideHistogramStatistic();
					SequenceInterface input = itemsListNucleotides.get(i);
					
					if(input instanceof AminoacidSequence) {
						AminoacidSequence input2 = (AminoacidSequence)input;
						Map<Character, Integer> statData = stat.getNucleotideHistogramStatistic(input2);
						
						String statDataText = "Statystyka aminokwasow dla sekwencji " + input.getSequenceName() + " \n\n";
						for(Entry<Character, Integer> statDataEntry : statData.entrySet()) {
							statDataText += statDataEntry.getKey() + ": " + statDataEntry.getValue() + "\n";
						}
						
						resultText.setText(statDataText);
					}
				}
			}
		});
		
		JMenuItem mntmObliczParyNukleotydow = new JMenuItem("Oblicz pary nukleotydow");
		mnObliczenia.add(mntmObliczParyNukleotydow);
		mntmObliczParyNukleotydow.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int i = itemsList.getSelectedIndex();
				
				if(i == -1) {
					JOptionPane.showMessageDialog(null, "nie wybrano sekwencji na liscie");
				}
				else {
					NucleotideSequenceNucleotidePairsStatistic stat = new NucleotideSequenceNucleotidePairsStatistic();
					SequenceInterface input = itemsListNucleotides.get(i);
					
					if(input instanceof NucleotideSequence) {
						NucleotideSequence input2 = (NucleotideSequence)input;
						Map<String, Integer> statData = stat.getNucleotidePairsStatistic(input2);
						
						String statDataText = "Statystyka par nukleotyow dla sekwencji " + input.getSequenceName() + " \n\n";
						for(Entry<String, Integer> statDataEntry : statData.entrySet()) {
							statDataText += statDataEntry.getKey() + ": " + statDataEntry.getValue() + "\n";
						}
						
						resultText.setText(statDataText);
					}
				}
			}
		});
		
		JMenuItem mntmObliczParyAminokwasow = new JMenuItem("Oblicz pary aminokwasow");
		mnObliczenia.add(mntmObliczParyAminokwasow);
		mntmObliczParyAminokwasow.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int i = itemsList.getSelectedIndex();
				
				if(i == -1) {
					JOptionPane.showMessageDialog(null, "nie wybrano aminokwasu na liscie");
				}
				else {
					AminoacidSequenceNucleotidePairsStatistic stat = new AminoacidSequenceNucleotidePairsStatistic();
					SequenceInterface input = itemsListNucleotides.get(i);
					
					if(input instanceof AminoacidSequence) {
						AminoacidSequence input2 = (AminoacidSequence)input;
						Map<String, Integer> statData = stat.getNucleotidePairsStatistic(input2);
						
						String statDataText = "Statystyka par aminokwasow dla sekwencji " + input.getSequenceName() + " \n\n";
						for(Entry<String, Integer> statDataEntry : statData.entrySet()) {
							statDataText += statDataEntry.getKey() + ": " + statDataEntry.getValue() + "\n";
						}
						
						resultText.setText(statDataText);
					}
				}
			}
		});
		
		mntmObliczNukleotyd.setEnabled(false);
		mntmObliczAminokwas.setEnabled(false);
		mntmObliczParyNukleotydow.setEnabled(false);
		mntmObliczParyAminokwasow.setEnabled(false);
		itemsList.addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				mntmObliczNukleotyd.setEnabled(false);
				mntmObliczAminokwas.setEnabled(false);
				mntmObliczParyNukleotydow.setEnabled(false);
				mntmObliczParyAminokwasow.setEnabled(false);

				int i = itemsList.getSelectedIndex();
				
				if(i != -1) {
					SequenceInterface input = itemsListNucleotides.get(i);

					if(input instanceof NucleotideSequence) {
						mntmObliczNukleotyd.setEnabled(true);
						mntmObliczParyNukleotydow.setEnabled(true);
					}
					if(input instanceof AminoacidSequence) {
						mntmObliczAminokwas.setEnabled(true);
						mntmObliczParyAminokwasow.setEnabled(true);
					}
				}
			}
		});
		
		JMenu mnPomoc = new JMenu("Pomoc");
		menuBar.add(mnPomoc);
		
		JMenuItem mntmAutorzy = new JMenuItem("Autorzy");
		mnPomoc.add(mntmAutorzy);
		mntmAutorzy.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String autorzyText = "Autorzy programu:\n\n"
						+ "Barbara Morcinek\n"
						+ "Weronika Tobor";
				JOptionPane.showMessageDialog(null, autorzyText);
			}
		});
	}

}
