package sequence.ui;

import java.util.Collections;
import java.util.List;

import javax.swing.AbstractListModel;
import javax.swing.JList;
import javax.swing.event.ListDataListener;

import sequence.data.NucleotideSequence;
import sequence.data.SequenceInterface;

class NucleotidesListModel extends AbstractListModel<NucleotidesListModel.NucleotideSequenceModelItem> {
	
	private JList targetList;
	private List<SequenceInterface> list;

	public NucleotidesListModel(JList targetList) {
		super();
		this.targetList = targetList;
		setList(Collections.emptyList());
	}
	
	public void setList(List<SequenceInterface> list) {
		this.list = list;
//	fireContentsChanged(this, 0, list.size()-1);
		targetList.updateUI();
	}

	@Override
	public void addListDataListener(ListDataListener l) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public NucleotideSequenceModelItem getElementAt(int index) {
		return new NucleotideSequenceModelItem(list.get(index));
	}

	@Override
	public int getSize() {
		return list.size();
	}

	@Override
	public void removeListDataListener(ListDataListener l) {
		// TODO Auto-generated method stub
		
	}
	
	public static class NucleotideSequenceModelItem {
		
		public SequenceInterface item;
		
		public NucleotideSequenceModelItem(SequenceInterface item) {
			this.item = item;
		}
		
		public String toString() {
			return item.getSequenceName();
		}
	}
}
